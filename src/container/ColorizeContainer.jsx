function ColorizeContainer(props) {
    const {
        children,
        color
    } = props;

    const textColor = (color)? color : 'green';

    return (
        <span style={{'color': textColor}}>{children}</span>
    )
}

export default ColorizeContainer;