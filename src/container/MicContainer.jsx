import { useState } from 'react';
import { ReactMic } from 'react-mic';

function MicContainer(props) {
  const {
    record
  } = props;

  function onData(recordedBlob) {
    console.log('chunk of real-time data is: ', recordedBlob);
  }

  function onStop(recordedBlob) {
    console.log('recordedBlob is: ', recordedBlob);
  }

  return (
    <div>
      <ReactMic
        record={record}
        className="sound-wave"
        onStop={onStop}
        onData={onData}
        strokeColor="#000000"
        backgroundColor="#FF4081" />
    </div>
  );
}

export default MicContainer;