import ColorizeContainer from './ColorizeContainer'

const punctuation_marks = /[ ,.!¡¿?]+/;

function split_at_index(value, index)
{
 return [value.substring(0, index), value.substring(index)];
}

function getWord(text, index) {
  const words = text.split(punctuation_marks)
  let word = '', startIndex = -1;

  if (index-1 <= words.length) {
    word = words[index-1];
    startIndex = text.indexOf(word);
  }

  return [word, startIndex];
}

function TextViewContainer(props) {
  let {
    textToPrint,
    index
  } = props;

  textToPrint = textToPrint.toLowerCase();
  
  let [word, startIndex] = getWord(textToPrint, index);
  let parts = split_at_index(textToPrint, startIndex);
  console.log([word, startIndex])

  if (word) {
    textToPrint = (
      <p>{parts[0]} <ColorizeContainer>{word}</ColorizeContainer> {split_at_index(parts[1], word.length)[1]}</p>
    );
  } else {
    textToPrint = <p>{textToPrint}</p>;
  }

  return (
    <div>
      {textToPrint}
    </div>
  );
}

export default TextViewContainer;