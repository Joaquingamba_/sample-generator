import { useRef, useState } from 'react';
import MicContainer from './container/MicContainer';
import TextViewContainer from './container/TextViewContainer';

function App() {
  const [index, setIndex] = useState(1);
  const [record, setRecord] = useState(false);
  const [timer, setTimer] = useState(null);
  const indexRef = useRef(index);
  indexRef.current = index;

  const speed = 500;
  const textToPrint = 'Hola, Como estas? Yo bien.';

  function onStart() {
    if (!timer) {
      setTimer(
        setInterval(
          () => {
            setIndex(indexRef.current + 1)
          }, 
          speed
        )
      );
    }
    setRecord(true);
  }

  function onStop() {
    clearInterval(timer);
    setTimer(null);
    setRecord(false);
  }

  function onReset() {
    onStop()
    setIndex(1);
  }

  return (
    <div>
      <div>
        <button className="button" onClick={onStart}>Start</button>
        <button className="button" onClick={onStop}>Stop</button>
        <button className="button" onClick={onReset}>Reset</button>
      </div>
      <TextViewContainer textToPrint={textToPrint} index={index} />
      <MicContainer record={record} />
    </div>
  );
}

export default App;
